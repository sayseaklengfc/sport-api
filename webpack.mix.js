const mix = require('laravel-mix');

require('dotenv').config();
const env = process.env;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    output: {
        chunkFilename:  'js/' + env.FRONT_JS_VERSION + '/[name].[chunkhash].js',
    },
})
.js('resources/js/football/football.js', 'public/js/'+ env.FRONT_JS_VERSION + '/football/js');
    // .sass('resources/sass/app.scss', 'public/css');
