<?php

use App\Http\Controllers\HomeController;
use App\Models\ApiBet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', function () {
    return view('football.index');
});

Route::post('/bet', [App\Http\Controllers\Web\BetController::class, 'bet']);


Route::get('bet/detail/{id}', function ($id){
    return ApiBet::where('id', $id)->with('details.match.league', 'child.details.match.league')->first();
});

Route::get('bet/detail/ref/{id}', function ($id){
    return ApiBet::where('ref_id', $id)->with('details.match.league', 'child.details.match.league')->first();
});

Route::get('getVersionBetMatch', function (){
    return DB::table('versions')->where('name','bet_match')->first()->version ?? 1;
});

Route::post('bet/active', [App\Http\Controllers\Web\BetController::class, 'active']);
