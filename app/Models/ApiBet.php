<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApiBet extends Model
{
    use HasFactory, SoftDeletes;

    public function details(){
        return $this->hasMany('App\Models\ApiBetDetail','bet_id','id');
    }

    public function child(){
        return $this->hasMany('App\Models\ApiBet', 'parent_id');
    }

}
