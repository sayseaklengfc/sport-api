<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Match extends Model
{
    use HasFactory;

    protected $fillable = [
        'date', 'bet_in', 'time', 'date_time', 'league_id', 'team_a_id', 'team_b_id', 'team_home_id', 'order_match'
    ];

    protected $appends = array("overUnder", "league", "teamAName", "teamBName");

    public function getTeamANameAttribute()
    {
        return Team::where('id', $this->team_a_id)->first()->name;
    }


    public function league(){
        return $this->hasOne('App\Models\League', 'id', 'league_id');
    }

    public function getTeamBNameAttribute()
    {
        return Team::where('id', $this->team_b_id)->first()->name;
    }

    public function getOverUnderAttribute()
    {
        return OverUnder::where('match_id', $this->id)->first();
    }

    public function getLeagueAttribute()
    {
        return League::where('id', $this->league_id)->first();
    }

    public function overUnder()
    {
        return $this->hasOne(OverUnder::class, 'match_id', 'id');
    }

    static function getMatchWithLeagueGBet(){
        return self::getMatchWithLeagueG(true);
    }

    static function getMatchWithLeagueG($isBet = false)
    {
        if(!$isBet){
            if(date('H') < 8){
                $matches = Match::with('overUnder')
                    ->orderBy('order_match')
                    ->whereDate('date_time', '>=',Carbon::today())
                    ->get();

            }else {
                $matches = Match::with('overUnder')
                    ->orderBy('order_match')
                    ->whereDate('bet_in', '>=',Carbon::today())
                    ->get();
            }

//            $matches = Match::with('overUnder')
//                ->orderBy('order_match')
//                ->whereDate('date_time', '>=',Carbon::today())
//                ->get();
        }
        else {
            $matches = Match::with('overUnder')
                ->orderBy('order_match')
                ->where('date_time', '>', Carbon::now()->addMinutes(5))
//                ->whereDate('bet_in','>=', Carbon::today())
                ->get();
//            return $matches;
        }

        if(date('H') < 9){
            $leaguesElo = LeagueOrder::whereDate('bet_in', '>=', Carbon::yesterday());
        }else {
            $leaguesElo = LeagueOrder::whereDate('bet_in', Carbon::today());
        }
        return $leaguesElo
            ->select('id', 'bet_in', 'league_id', 'league_order_number')
            ->orderBy('league_order_number')
            ->get()->map(function ($leagues) use ($matches) {
                $m = array();
                foreach ($matches as $value) {
                    if ($value->league->id == $leagues->league_id) {
                        $leagues['name'] = $value->league->name;
                        array_push($m, $value);
                    }
                }

                $leagues['matches'] = $m;
                return $leagues;
            });
    }

    static function getMatchListMember(){

        $matchesYesterday = Match::with('overUnder')
            ->orderBy('order_match')
            ->where('date_time', '>', Carbon::now()->addMinutes(5))
            ->where('bet_in', Carbon::yesterday())
            ->get();

        $matchesToday = Match::with('overUnder')
            ->orderBy('order_match')
            ->where('date_time', '>', Carbon::now()->addMinutes(5))
            ->where('bet_in', Carbon::today())
            ->get();

        $responseYesterday = [];
        if($matchesYesterday != null && count($matchesYesterday) > 0){
            $responseYesterday = LeagueOrder::whereDate('bet_in', Carbon::yesterday())
                ->select('id', 'bet_in', 'league_id', 'league_order_number')
                ->orderBy('league_order_number')
                ->get()->map(function ($leagues) use ($matchesYesterday) {
                    $m = array();
                    foreach ($matchesYesterday as $value) {
                        if ($value->league->id == $leagues->league_id) {
                            $leagues['name'] = $value->league->name;
                            array_push($m, $value);
                        }
                    }

                    $leagues['matches'] = $m;
                    if(count($leagues['matches']) > 0){
                        return $leagues;
                    }else {
                        return null;
                    }
                });
            $responseYesterday = array_values(array_filter($responseYesterday->toArray()));
        }

        $responseToday = [];
        if($matchesToday != null && count($matchesToday) > 0){
            $responseToday =  LeagueOrder::whereDate('bet_in', Carbon::today())
                ->select('id', 'bet_in', 'league_id', 'league_order_number')
                ->orderBy('league_order_number')
                ->get()->map(function ($leagues) use ($matchesToday) {
                    $m = array();
                    foreach ($matchesToday as $value) {
                        if ($value->league->id == $leagues->league_id) {
                            $leagues['name'] = $value->league->name;
                            array_push($m, $value);
                        }
                    }

                    $leagues['matches'] = $m;
                    return $leagues;
                });
        }


        return [
            "date_today" => date('Y-m-d'),
            "date_yesterday" => Carbon::yesterday()->format('Y-m-d'),
            "match_today" => $responseToday,
            "match_yesterday" => $responseYesterday
        ];
    }


    static function getMatchWithLeagueGYesterday($isBet = false)
    {
        $matches = Match::with('overUnder')
            ->orderBy('order_match')
            ->where('date_time', '>', Carbon::now()->addMinutes(5))
                ->whereDate('bet_in', Carbon::yesterday())
            ->get();

        return LeagueOrder::whereDate('bet_in', Carbon::yesterday())
            ->select('id', 'bet_in', 'league_id', 'league_order_number')
            ->orderBy('league_order_number')
            ->get()->map(function ($leagues) use ($matches) {
                $m = array();
                foreach ($matches as $value) {
                    if ($value->league->id == $leagues->league_id) {
                        $leagues['name'] = $value->league->name;
                        array_push($m, $value);
                    }
                }

                $leagues['matches'] = $m;
                return $leagues;
            });
    }

    static function getMatch()
    {
        return Match::with('overUnder')->orderBy('date_time', 'desc')->where('bet_in', date('Y-m-d'));
    }

    public function bets(){
        return $this->hasManyThrough(
            'App\Model\Bet',
            'App\Model\BetDetail',
            'match_id', // Foreign key on bet_details table...
            'id', // Foreign key on bets table...
            'id', // Local key on matches table...
            'bet_id' // Local key on bet_details table...
        );
    }
}
