<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;


class Bet extends Model
{
    use HasFactory, SoftDeletes;

    static function getBet(){
        return Bet::where('user_id', Auth::id());
    }

    public function betDetail(){
        return $this->hasMany('App\Model\BetDetail','bet_id','id');
    }

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function userWithName(){
        return $this->hasOne('App\User', 'id', 'user_id')->select(array('id', 'username'));
    }

    public function child(){
        return $this->hasMany('App\Model\Bet', 'parent_id');
    }


    public function scopeParent($query){
        return $query->where('parent_id', 0);
    }
}
