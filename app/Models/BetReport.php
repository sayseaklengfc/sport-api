<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BetReport extends Model
{
    use HasFactory;

    protected $table = 'bets';
    protected $appends = array("sumBetAmountChild", "calculateCount", "totalWinLose");

    static function getBetReport(){
        return Bet::where('user_id', Auth::id())->where('is_calculated', 0)->where('parent_id', 0);
    }

    public function getChildAttribute(){
        return Bet::where('parent_id', $this->id)->get();
    }

    public function getSumBetAmountChildAttribute(){
        return Bet::where('parent_id', $this->id)->sum('bet_amount');
    }

    public function getCalculateCountAttribute(){
        // plus 1 size with parent
        $size = Bet::where('parent_id', $this->id)->count() + 1;
        $sizeCalculate = Bet::where('parent_id', $this->id)->sum('is_calculated');
        return $sizeCalculate . '/' . $size;
    }

    public function getTotalWinLoseAttribute(){
        $betChild       = Bet::where('parent_id', $this->id);
        $totalBetAmount = $this->bet_amount + $betChild->sum('bet_amount');
        $winLose        = $this->win_lose_amount + $betChild->sum('win_lose_amount');
        return $winLose - $totalBetAmount;
    }

    public function betDetail(){
        return $this->hasMany(BetDetail::class,'bet_id','id');
    }
}
