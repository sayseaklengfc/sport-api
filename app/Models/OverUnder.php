<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OverUnder extends Model
{
    use HasFactory;

    protected $fillable = [
        'match_id', 'handi', 'handi_p_team_a', 'handi_p_team_b',
        'over_under', 'over_p', 'under_p'
    ];
}
