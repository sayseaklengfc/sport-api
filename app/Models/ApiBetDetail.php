<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApiBetDetail extends Model
{
    use HasFactory, SoftDeletes;

    public function match(){
        return $this->hasOne('App\Models\Match', 'id','match_id');
    }

}
