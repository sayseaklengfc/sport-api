<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueOrder extends Model
{
    use HasFactory;

    protected $fillable = [
        'bet_in', 'league_id', 'eague_order_number'
    ];
}
