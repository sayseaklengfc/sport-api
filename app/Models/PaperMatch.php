<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaperMatch extends Model
{
    use HasFactory;

    protected $fillable = [
		'date', 'total_league','total_match', 'total_set_result',
	];
}
