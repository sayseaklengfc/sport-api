<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class BetDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $appends = array( "betTeamName");

    public function bet(){
        return $this->hasOne('App\Model\Bet', 'id','bet_id');
    }

    public function match(){
        return $this->hasOne('App\Model\Match', 'id','match_id');
    }

    public function getBetTeamNameAttribute(){
        return Team::where('id', $this->bet_team_id)->first()->name;
    }
}
