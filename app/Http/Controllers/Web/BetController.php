<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ApiBet;
use App\Models\ApiBetDetail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BetController extends Controller
{

    function active(Request $request){
        return $request;
    }

    function bet(Request $request){
        $betAmount = $request->amount;
        if($request->is_multi){
            $betAmount =  $request->amount * (count($request->bets) + 1);
        }

        $betId = DB::transaction(function() use ($request, $betAmount){
            if($request->is_multi){
                $betId =  $this->multiBet($request);
            }else {
                $betId =  $this->singleBet($request, 0 , 0, $request->bet_type);

            }
            return $betId;
        });
        if($request->is_multi){
            $totalOdd = ApiBet::where('id', $betId)->orWhere('parent_id', $betId)->sum('total_odd');
        }else {
            $totalOdd = intval(($request->total_odd*100))/100;
        }

        return [
            "bet_id" => $betId,
            "amount" => $request->amount,
            "total_amount" => $betAmount,
            "total_ticket" => ApiBet::where('id', $betId)->orWhere('parent_id', $betId)->count(),
            "total_odd" => ($totalOdd * 1),
            "is_multi" => $request->is_multi,
            "schedule_date" => $request->bet_in_date,
            "bet_type" => $request->bet_type,
        ];
    }

    private function calculateMultiOdd($items){
        $sumOdd = 1;
        foreach ($items as $item){
            $sumOdd = $sumOdd * ($item['bet_win_p'] );
        }
        return number_format($sumOdd, 2);
    }

    function get_array_combination($arr) {
        $results = array(array( ));

        foreach ($arr as $values)
            foreach ($results as $combination)
                array_push($results, array_merge($combination, array($values))); // Get new values and merge to your previous combination. And push it to your results array
        return $results;
    }

    private function multiBet(Request $request){
        $betType = $request->bet_type;
        $parentId = $this->singleBet($request, 1, 0, $betType);
        $set = $request->bets;
        $set2 = $this->get_array_combination($set);
        $final_array = array();
        foreach($set2 as $item){
            if(count($item) == count($request->bets) - 1 ){
                array_push($final_array, $item);
            }
        }

        foreach ($final_array as $item){
            $request->bets = $item;
            $request->total_odd = round($this->calculateMultiOdd($item), 5);
            $this->singleBet($request, 1, $parentId, $betType);
        }
        return $parentId;
    }

    private function singleBet(Request $request, $isMulti = 0, $parentId = 0, $betType = '1x1'){
        $bet                = new ApiBet();
        $bet->website_id    = 1;
        $bet->user_id       = $request->user_id;
        $bet->bet_time      = Carbon::now();
        $bet->bet_amount    = $request->amount;
        $bet->total_odd     = intval(($request->total_odd*100))/100;
        $bet->total_bet_odd = intval(($request->total_odd*100))/100;
        $bet->is_multi      = $isMulti;
        $bet->parent_id     = $parentId;
        $bet->type          = $betType;
        $bet->schedule_date = $request->bet_in_date;
        $bet->is_active = 0;
        $bet->save();
        $this->saveBetDetail($request, $bet->id);
        $bet->ref_id = $bet->id;
        $bet->save();
        return $bet->id;
    }

    private function saveBetDetail(Request $request, $betId){
        foreach ($request->bets as $item){
            $bet_detail                 = new ApiBetDetail();
            $bet_detail->bet_id         = $betId;
            $bet_detail->match_id       = $item['bet_match_id'];
            $bet_detail->bet_team_id    = $item['team_id'];
            $bet_detail->input_id       = $item['bet_input'];
            $bet_detail->odd            = $item['odd'];
            $bet_detail->bet_type       = $item['type'];
            $bet_detail->bet_win_p      = $item['bet_win_p'];
            $bet_detail->save();
        }
    }
}
