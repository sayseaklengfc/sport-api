require('./../bootstrap');


import Vuetify from "vuetify";
import Vuex from "vuex";
import VueRouter from 'vue-router';
import store from "./store/index";


window.Vue = require('vue');
window.Event = new Vue();
Vue.prototype.$eventBus = new Vue();

Vue.use(Vuex);
Vue.use(Vuetify);
Vue.use(VueRouter);



axios.interceptors.request.use( x => {
    // to avoid overwriting if another interceptor
    // already defined the same object (meta)
    x.meta = x.meta || {}
    x.meta.requestStartedAt = new Date().getTime();
    return x;
});

Vue.component('main-component', require('./page/Index.vue').default);

const routes = [

];

const router = new VueRouter({
    mode: 'history',
    routes,
    linkActiveClass: "active",
    linkExactActiveClass: "exact-active",
});

const app = new Vue({
    el: '#app',
    store,
    router,
    vuetify: new Vuetify({
        theme: {
            themes: {
                light: {
                    primary: '#f48002',
                    secondary: '#fd9e0f',
                    anchor: '#fd9e0f',
                },
            },
        },
    }),
});
