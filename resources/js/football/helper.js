import moment from "moment";

const BigNumber = require('bignumber.js');
var helper ={
    getOdd(inputId, overUnder){
        var odd = 0;
        var isRotate = false;
        if (overUnder.handi < 0) isRotate = true;

        if (inputId >= 1 && inputId <= 500) {
            odd = overUnder.handi_p_team_a;
        } else if (inputId >= 501 && inputId <= 1000) {
            odd = overUnder.handi_p_team_b;
        } else if (inputId >= 1001 && inputId <= 1500) {
            odd = overUnder.over_p;
        } else if (inputId >= 1501 && inputId <= 2000) {
            odd = overUnder.under_p;
        }
        var percentageOdd = new BigNumber(odd).dividedBy(100).plus(1);
        return percentageOdd.toNumber();
    },

    getTeamIdFromInput(inputId){
        var refactorId = 0;
        if (inputId >= 1 && inputId <= 500) {
            refactorId = inputId;
        } else if (inputId >= 501 && inputId <= 1000) {
            refactorId = inputId - 500;
        } else if (inputId >= 1001 && inputId <= 1500) {
            refactorId = inputId - 1000;
        } else if (inputId >= 1501 && inputId <= 2000) {
            refactorId = inputId - 1500;
        }
        return refactorId;
    },

    filterMergeMatch(matchGroups, matchId){
        var nestTo2Dimension = matchGroups.map(matchGroup => matchGroup.matches);
        var array = nestTo2Dimension.reduce(function (prev, next) {
            return prev.concat(next);
        });
        return array.find(match => match.order_match == matchId);
    },
    getOddFromInputId2(match, inputId){

        if (inputId >= 1 && inputId <= 500) {
            return match.overUnder.handi * -1;
        } else if (inputId >= 501 && inputId <= 1000) {
            return match.overUnder.handi * 1;
        } else {
            return match.overUnder.over_under * 1;
        }
    },
    getOddFromInputId(match, inputId){

        if (inputId >= 1 && inputId <= 500) {
            return match.over_under.handi * -1;
        } else if (inputId >= 501 && inputId <= 1000) {
            return match.over_under.handi * 1;
        } else {
            return match.over_under.over_under * 1;
        }
    },
    getOddWinPercentage(match, id) {
        var overUnder = match.over_under;
        var odd = 0;
        var isRotate = false;
        if (overUnder.handi < 0) isRotate = true;

        if (id >= 1 && id <= 500) {
            odd = overUnder.handi_p_team_a;
        } else if (id >= 501 && id <= 1000) {
            odd = overUnder.handi_p_team_b;
        } else if (id >= 1001 && id <= 1500) {
            odd = overUnder.over_p;
        } else if (id >= 1501 && id <= 2000) {
            odd = overUnder.under_p;
        }
        var percentageOdd = new BigNumber(odd).dividedBy(100).plus(1);
        return percentageOdd.toNumber();
    },
    getTypeBet(inputId) {
        if (inputId >= 1 && inputId <= 1000) {
            return "HDP";
        } else if (inputId >= 1001 && inputId < 1501) {
            return "OVER";
        } else {
            return "UNDER";
        }
    },
    getTeamIdBet(match, inputId){
        if ((inputId >= 1 && inputId <= 500) || (inputId >= 1001 && inputId <= 1500)) {
            return match.team_a_id;
        } else {
            return match.team_b_id;
        }
    },
    formatDate(time){
        return moment(time).format('DD/MM/YYYY hh:mm a');
    },
    formatDatePicker(date){
        if (!date) return null

        const [year, month, day] = date.split('-')
        return `${day}/${month}/${year}`
    },
    parseDatePicker(date){
        if (!date) return null
        const [month, day, year] = date.split('/')
        return `${year}-${month.padStart(2, '0')}-${day.padStart(2, '0')}`
    },
    getDisplayHandicap(value){
        if(value == null) return '';
        value = (value + '').replace("-","");
        var removeZeroDecimal = value * 1;
        if(!removeZeroDecimal.toString().includes('.')) return removeZeroDecimal;
        if(!removeZeroDecimal.toString().split(".")[1]) return "";
        var decimal = removeZeroDecimal.toString().split(".")[1];
        if(decimal == 0 || decimal == 5) return removeZeroDecimal;
        var positive = value.split(".")[0];
        if(decimal == 25){
            return positive + "/" + positive+ ".5" ;
        }
        if(decimal == 75){
            return positive + ".5/" + (parseInt(positive) + 1) ;
        }
        return 'wrong';
    },
    groupBy(array, key) {
        const result = {};
        array.forEach(item => {
            if (!result[item[key]]) {
                result[item[key]] = []
            }
            result[item[key]].push(item)
        });
        return result
    },
    toFixed(num, fixed) {
        if(num instanceof BigNumber && num.isNaN()) return "0.00";
        if(num == null) return "0.00";
        var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (fixed || -1) + '})?');
        var numPrematch = num.toString().match(re)[0];
        if(numPrematch % 1 == 0 && !numPrematch.includes(".00")) return numPrematch + ".00";
        else if(numPrematch % 0.1 == 0) return numPrematch + ".0";
        return numPrematch;
        // rounded.value = with2Decimals
    },
    getResultWinDisplay(betDetail){
        if(betDetail.is_calculated === 0){
            return "";
        }
        // to prevent round up check 3 condition
        var winHalfOdd = (betDetail.bet_win_p  - 1) / 2;
        if(betDetail.bet_win_p_calculated == winHalfOdd + 1){
            return "win 0.5"
        }
        else if(betDetail.bet_win_p_calculated == Number(winHalfOdd + 1).toFixed(5)){
            return "win 0.5"
        }
        else if(Number(betDetail.bet_win_p_calculated).toFixed(2) == Number(winHalfOdd + 1).toFixed(2)){
            return "win 0.5"
        }
        else if (betDetail.bet_win_p_calculated == 0.5) {
            return 'Lost 0.5';
        } else if (betDetail.bet_win_p_calculated == 1 && betDetail.match.team_a_result < 0) {
            return 'Refund';
        } else if (betDetail.bet_win_p_calculated == 1 ) {
            return 'Draw';
        } else if (betDetail.bet_win_p_calculated > 1) {
            return 'Win';
        } else if (betDetail.bet_win_p_calculated == 0) {
            return 'Lost';
        }
    },
    getMonday(d) {
        d = new Date(d);
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
        return new Date(d.setDate(diff));
    },
    setFocusOnInput(inputName) {
        /**
         * @see https://vuejs.org/v2/api/#vm-el
         * @see https://vuejs.org/v2/api/#vm-refs
         */
            // you could just call this.$refs[inputName].focusInput() but i'm not shure if it belongs to the public API
        let inputEl = this.$refs[inputName].$el.querySelector('input')
        // console.log(inputEl.focus) // <== See if `focus` method avaliable
        inputEl.focus() //  <== This time the focus will work properly
    }
};

export default helper;
