export const API_GET_MATCH_GROUP = "/getMatchGroup";
export const API_GET_MATCH_GROUP_BET_YESTERDAY = "/api/getMatchListYesterday";
export const API_GET_MATCH_GROUP_BET = "/getMatchListBet";
export const API_GET_MATCH_GROUP_BET2 = "/getMatchListBet2";
export const API_GET_CURRENT_USER = "/currentUser";
export const API_CURRENT_OUTSTANDING = "/outstanding";

export const OPEN_BET_DETAIL_DIALOG ='open_bet_detail_dialog';
export const OPEN_BOTTOM_SHEET_STATEMENT ='open_bottom_sheet_statement';
export const OPEN_UPDATE_MATCH_DIALOG = 'open_update_match_dialog';
export const OPEN_RESET_RESULT_MATCH_DIALOG = 'open_reset_result_match_dialog';

export const OPEN_DRAWER_OUTSTANDING = "open_drawer_outstanding";
export const OPEN_DIALOG_CHANGE_PASSWORD = "open_dialog_change_password";
export const OPEN_DIALOG_PLACE_BET_INFORM = "open_place_dialog_bet_inform";
export const CONFIRMED_PLACE_BET_INFORM = "confirmed_dialog_bet_inform";

export const LOAD_WINLOSE_MEMBER_DATA = "load_winlose_data";

export const WIN_LOSE_SHOW_BET_DETAIL = "show_bet_detail_winlose";

export const MATCH_BET_UPDATED = "match_bet_updated";
export const MATCH_RESULT_RESET = "match_result_reseted";

export const GET_MATCH_TODAY = "get_match_today";
export const GET_MATCH_YESTERDAY = "get_match_yesterday";
export const GET_MATCH_TOMORROW = "get_match_tomorrow";


export const UPDATED_MATCH_RESULT = "updated_match_result";
