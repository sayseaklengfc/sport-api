const state = {
    userId: null,
    balance: null,
    minBet: null,
    maxBet: null,
    currentUserLevelId: 4,

    outStandingData: null,
    outStandingCount: null,
    outStandingAmount: null,

    matchGroupLoading: false,
    matchGroup: [],
    matchGroupYesterday: [],
    matchGroupToday: [],
    matchTimeToday: null,
    matchTimeYesterday: null,
    isMatchYesterdaySelected: false,

    betSelectedIds: [],
    betMatchSelected: [],

    betMatchVersion: 1,
};
export default state;
