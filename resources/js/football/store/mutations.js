import helper from "../helper";
export default {
    ['UPDATE_USER'](state, user){
        state.balance = user.balance;
        state.minBet = user.min_bet;
        state.maxBet = user.max_bet;
        state.userId = user.id;
        state.currentUserLevelId = user.level_id;
    },
    ['MATCH_GROUP_LOADING'](state, isLoading){
        state.matchGroupLoading = isLoading;
    },
    ['GET_MATCH_GROUP'](state, matchGroup){
        state.matchGroup = matchGroup;
        // state.matchGroup = matchGroup.match_today;
        // state.matchGroupYesterday = matchGroup.match_yesterday;
        // state.matchGroupToday = matchGroup.match_today;
        // state.matchTimeToday = matchGroup.date_today;
        // state.matchTimeYesterday = matchGroup.date_yesterday;
    },
    ['ON_ADD_SELECTED_ID'](state, payload){
        if(payload.match){
            state.betSelectedIds.push(payload.inputSelectedId);
            state.betMatchSelected.push({
                bet_input: payload.inputSelectedId,
                bet_match_id: payload.match.id,
                type: helper.getTypeBet(payload.inputSelectedId),
                bet_win_p: helper.getOddWinPercentage(payload.match, payload.inputSelectedId),
                odd: helper.getOddFromInputId(payload.match, payload.inputSelectedId),
                match: payload.match,
                team_id: helper.getTeamIdBet(payload.match, payload.inputSelectedId)
            })
        }
    },
    ['ON_REMOVE_SELECTED_ID'](state, payload){
        state.betSelectedIds = state.betSelectedIds.filter(selectedId => selectedId != payload.inputSelectedId);
        state.betMatchSelected = state.betMatchSelected.filter(bet => bet.bet_match_id != payload.match.id);
    },
    ['ON_SUBMIT_CLEAR'](state, payload){
        // console.log("clear");
        state.betSelectedIds = [];
        state.betMatchSelected = [];
    }
}
