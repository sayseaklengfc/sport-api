import axios from 'axios';
import moment from 'moment';
import {
    API_GET_MATCH_GROUP,
    API_GET_CURRENT_USER,
    API_CURRENT_OUTSTANDING,
    API_GET_MATCH_GROUP_BET,
    MATCH_BET_UPDATED, API_GET_MATCH_GROUP_BET2
} from '../api';
export default {
    switchMatchToday({dispatch, commit, state}){
        state.isMatchYesterdaySelected = false;
        dispatch('onSubmitClear');
    },
    switchMatchYesterday({dispatch, commit, state}){
        state.isMatchYesterdaySelected = true;
        dispatch('onSubmitClear');
    },
    removeMatchTimeOut({dispatch, commit, state}){
        var hasUpdated = false;
        // old
        state.matchGroup.forEach(function (matchGroup, i){
            var matches = [];
            matchGroup.matches.forEach(function (match){
                var matchTimeMoment = moment(match.date_time);
                if(matchTimeMoment.isAfter(moment())){
                    matches.push(match);
                }
            });
            if(JSON.stringify(state.matchGroup[i].matches) !== JSON.stringify(matches) ) {
                state.matchGroup[i].matches = matches;
                hasUpdated = true;
            }

        });

        //new
        state.matchGroupToday.forEach(function (matchGroup, i){
            var matches = [];
            matchGroup.matches.forEach(function (match){
                var matchTimeMoment = moment(match.date_time);
                if(matchTimeMoment.isAfter(moment())){
                    matches.push(match);
                }
            });
            if(JSON.stringify(state.matchGroupToday[i].matches) !== JSON.stringify(matches) ) {
                state.matchGroupToday[i].matches = matches;
                hasUpdated = true;
            }
        });

        state.matchGroupYesterday.forEach(function (matchGroup, i){
            var matches = [];
            matchGroup.matches.forEach(function (match){
                var matchTimeMoment = moment(match.date_time);
                if(matchTimeMoment.isAfter(moment())){
                    matches.push(match);
                }
            });
            if(JSON.stringify(state.matchGroupYesterday[i].matches) !== JSON.stringify(matches) ) {
                state.matchGroupYesterday[i].matches = matches;
                hasUpdated = true;
            }
        });

        if(hasUpdated){
            dispatch('onSubmitClear');
            this._vm.$eventBus.$emit(MATCH_BET_UPDATED, "Matches updated!");
        }
    },
    getMatchVersion({commit, state}){
        axios.get('/api/getVersionBetMatch').then(res =>{
            state.betMatchVersion = res.data;
        }).catch();
    },
    getMatchIfUpdated({dispatch, commit, state}){
        axios.get('/api/getVersionBetMatch').then(res =>{
            if(res.data > state.betMatchVersion){
                state.betMatchVersion = res.data;
                // dispatch('getMatchGroup');
                dispatch('getMatchGroup2')
                dispatch('onSubmitClear');
                this._vm.$eventBus.$emit(MATCH_BET_UPDATED, "Matches updated!");
            }
        }).catch();
    },
    getCurrentUser({commit, state}){
        state.balance = 0
        return axios.get(API_GET_CURRENT_USER).then(res => {
            commit('UPDATE_USER', res.data);
        }).catch();
    },

    getOutStanding({commit, state}){
        state.outStandingAmount = null;
        state.outStandingData = null;

        return axios.post(API_CURRENT_OUTSTANDING).then(res => {
            state.outStandingAmount = res.data.amount;
            state.outStandingCount = res.data.count;
            state.outStandingData = res.data.data;
        }).catch();
    },
    getMatchGroup({commit}){
        commit('MATCH_GROUP_LOADING', true);
        return axios.get(API_GET_MATCH_GROUP_BET).then(res => {
            commit('GET_MATCH_GROUP', res.data);
            commit('MATCH_GROUP_LOADING', false);
        }).catch();
    },
    getMatchGroup2({commit, state}){
        commit('MATCH_GROUP_LOADING', true);
        axios.get("http://sport889.com/api/getMatchListBet3").then(res => {
            state.matchGroupToday = res.data.match_today;
            state.matchGroupYesterday = res.data.match_yesterday;
            state.matchTimeToday = res.data.date_today;
            state.matchTimeYesterday = res.data.date_yesterday;
        }).catch(err => {

        }).then(() => {
            commit('MATCH_GROUP_LOADING', false);
        });
        // commit('MATCH_GROUP_LOADING', true);
        // return axios.get(API_GET_MATCH_GROUP_BET).then(res => {
        //     commit('GET_MATCH_GROUP', res.data);
        //     commit('MATCH_GROUP_LOADING', false);
        // }).catch();
    },
    getMatchGroupYesterday({commit}){

    },
    getMatchGroupTomorrow({commit}){

    },
    onSubmitClear({commit}){
        commit('ON_SUBMIT_CLEAR');
    },
    onSubmitClickId({commit, state}, payload){
        if(state.betSelectedIds.includes(payload.inputSelectedId)){
            // commit('ON_REMOVE_SELECTED_ID', payload);
        }else if(state.betMatchSelected.filter(item => item.bet_match_id == payload.match.id).length > 0){
           console.log("item already bet");

        }else {
            commit('ON_ADD_SELECTED_ID', payload);
        }
    },

    onSubmitInputIdAction({commit, state}, payload){
        if(state.betSelectedIds.includes(payload.inputSelectedId)){
            // commit('ON_REMOVE_SELECTED_ID', payload);
        }else {
            commit('ON_ADD_SELECTED_ID', payload);
        }
    },

    onRemoveBetInputIdBet({commit, state}, payload){
        commit('ON_REMOVE_SELECTED_ID', payload);
    },
    updateUser({commit, state}, payload){
        state.userId = payload.user_id;
        state.balance = payload.balance ?? 0;
        state.minBet = payload.setting.min_bet_amount;
        state.maxBet = payload.setting.max_bet_amount;
    }
}
