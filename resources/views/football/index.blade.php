<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Doc</title>

    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!-- Styles -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.8.95/css/materialdesignicons.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">

    {{--    <link href="{{ asset('css/frontend.css?v=1') }}" rel="stylesheet">--}}
{{--    <link href="{{ asset('css/bet_index.css?v=4') }}" rel="stylesheet">--}}
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link href="{{asset('css/table_list_admin.css')}}" rel="stylesheet">
    <link href="{{ asset('css/bet_index.css?v=12') }}" rel="stylesheet">
    <style>
        * {
            font-family: "Helvetica Neue", sans-serif;
        }
    </style>
</head>
<body style="margin: 0; padding: 0">
    <v-app id="app">
        <main-component></main-component>
    </v-app>
    <script src="{{ asset( 'js/' .env('FRONT_JS_VERSION', 'v1.0') .'/football/js/football.js?v=1') }}"></script>
</body>
</html>
